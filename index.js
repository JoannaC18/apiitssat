const express = require('express');
const fs = require('fs');
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
const port = 3000;
const manejaGet = (req, res) => {
    console.log("Estoy en la pagina home");
    res.json({"saludo":'Hola mundo desde Node!'});
}
const nombre= (req, rest) => {
    console.log("Estoy escribiendo mi nombre");
    rest.json({"Nombre":'Joanna Guadalupe Carvajal Garcia'});
}

app.get('/home', manejaGet);
app.get('/joanna', nombre);

const readFile = (err, data) => {
    if(err) console.log('Hubo un error');
    let info = JSON.parse(data);
    console.log(info);
    return info;
}

app.get('/people', (req, res) => {
    let data = fs.readFileSync('database/table.json');
    let info = JSON.parse(data);
    console.log('Leimos el archivo');
    console.log(info);
    res.json({response: info}) 
})

app.get('/people/:id', (req, res) =>{
    //guardaremos el id con que se realizo la peticion
    let identifier = req.params.id;
    // leemos la inormacion de la base de datos
    let data = fs.readFileSync('database/table.json');
    let obJSON = JSON.parse(data);
    // tratamos de buscar el identificador en la base de datos
    let response = null;
    for(let i =0; i < obJSON.length; i++){
        if (obJSON[i].id == identifier){
            response= obJSON[i];
            break;
        }
    }
    //regreso lo que encontre
    if(response == null){
    res.status(404).send();
    return ;
    }
    res.json(response);
})
//agregando una nueva persona
app.post('/people', (req, res) => {
    console.log(req.body);
    let person = req.body;
    let data = fs.readFileSync('database/table.json');
    let info = JSON.parse(data);
    //validacion si el usuario ya existe en nuestra base de datos y si es asi entonces marca un mensaje que ya existe
    for(let k =0; k < info.length; k++){
        if (info [k].id == person.id){
            res.status(400).json({mensaje: 'El ususario ya existe'}); //mensaje
            return ;
        }
    }
    info.push(person);
    fs.writeFileSync('database/table.json', JSON.stringify(info));
    res.status(201).json(person);
})

app.listen(port,() => {
    console.log("El servidor esta escuchando en la url http://localhost:", port);
} )

